package anu.ericm.spaceinvader2;

/**
 * Created by ericm on 27/03/17.
 */

public interface GameOver {

    public void gameOver();
}

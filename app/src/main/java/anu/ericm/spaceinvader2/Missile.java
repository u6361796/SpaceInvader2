
package anu.ericm.spaceinvader2;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Missile - this is a simple missle sprite.  Note the missiles are shot from the space ship.
 * Eric McCreath 11/03/16.
 */
public class Missile extends Sprite {

    public static final float MISSILEHEIGHT = (1.0f / 50.0f);

    public Missile(Pos p) {
        pos = new Pos(p);
    }

    public void draw(Canvas c , Paint p) {
        int h = c.getHeight();
        int w = c.getWidth();

        float xc = pos.x * w;
        float yc = pos.y * h;
        float mh = MISSILEHEIGHT * w;
        c.drawLine(xc, yc, xc, yc - mh, p);
    }
}

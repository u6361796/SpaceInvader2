
package anu.ericm.spaceinvader2;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static android.support.v4.app.ActivityCompat.startActivity;

/**
 * SpaceView - this is the view the holds the main game
 * Eric McCreath 11/03/16.
 */


public class SpaceView extends View implements View.OnTouchListener ,  Runnable{
    public static final int STEPDELAY = 100;
    public static final int DEFAULTCOLOUR = Color.BLACK;
    public static final float DEFAULTPENWIDTH = 3.0f;
    Paint paint;
    Handler repaintHandler;
    Game game;
    ArrayList<GameOver> observers ;

    public SpaceView(Context context, AttributeSet attrs) {
        super(context, attrs);

        paint = new Paint();
        paint.setColor(DEFAULTCOLOUR);
        paint.setStrokeWidth(DEFAULTPENWIDTH);
        this.setOnTouchListener(this);
        observers = new ArrayList<GameOver>();
        game = new Game();

        repaintHandler = new Handler();
        repaintHandler.postDelayed(this, STEPDELAY);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        float w = (float) v.getWidth();
        game.touch(event.getX() / w);
        return true;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        game.draw(canvas, paint);
    }


    // step the view forward by one step - true is returned if more steps to go
    public boolean step() {
        game.step();
        if (game.hasWon() || game.shipHit()) {
           /* Context context = this.getContext();
            while (!(context instanceof GameActivity))
                context = ((GameActivity) context).getBaseContext();
            ((GameActivity) context).endActivity(game.hasWon() ? "You Win !!" : "You Lost :(");*/
           notifyGameOver();
            return false;
        }
        this.invalidate();
        return true;
    }

    private void notifyGameOver() {
        for (GameOver o : observers) o.gameOver();
    }


    @Override
    public void run() {
        if (step()) {
            repaintHandler.postDelayed(this, SpaceView.STEPDELAY);
        }
    }

    public void registerGameOver(GameOver gameover) {
        observers.add(gameover);
    }
}

package anu.ericm.spaceinvader2;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Bomb - bombs that drops down from aliens.
 * Eric McCreath on 11/03/16.
 */
public class Bomb extends Sprite {

    public static final float BOMBRADIUS = (1.0f / 100.0f);

    public Bomb(Pos p) {
        pos = new Pos(p);
    }

    // draw the bomb
    public void draw(Canvas c , Paint p) {
        int h = c.getHeight();
        int w = c.getWidth();

        float xc = pos.x * w;
        float yc = pos.y * h;
        float r = w * BOMBRADIUS;
        c.drawCircle(xc, yc, r, p);
    }
}

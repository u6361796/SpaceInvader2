
package anu.ericm.spaceinvader2;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/**
 * Game - this is the model of the main game.
 * Eric McCreath 11/03/16.
 */
public class Game {

    static final int ALIENROWS = 3;
    static final int ALIENCOLUMNS = 10;
    public static final float MAXXY = 1.0f;
    public static final float MINXY = 0.0f;


    public static final float MISSILESTEP = 0.06f;

    private SpaceShip spaceShip;
    private Aliens aliens;
    private Bombs bombs;
    private Missiles missiles;

    private boolean shipHit;


    public Game() {
        spaceShip = new SpaceShip();
        aliens = Aliens.gridAliens(ALIENCOLUMNS, ALIENROWS);
        bombs = new Bombs();
        missiles = new Missiles();

        shipHit = false;
    }

    // draw all the game
    public void draw(Canvas canvas, Paint paint) {
        spaceShip.draw(canvas, paint);
        aliens.draw(canvas, paint);
        bombs.draw(canvas, paint);
        missiles.draw(canvas, paint);
    }

    // move the game forward one step
    public void step() {

        aliens.step();

        // randomly add bomb from aliens
        bombs.addAll(aliens.getBombs());

        // move the bombs - remove ones that are off the screen
        bombs.step();

        // move the missiles - remove ones that are off the screen
        missiles.step();

        // remove the aliens hit by missiles
        aliens.removeHit(missiles);


        // check if a bomb has hit the ship
        for (Bomb b : bombs) {
            if (spaceShip.hitby(b)) shipHit = true;
        }
    }

    public boolean hasWon() {
        return !shipHit && aliens.size() == 0;
    }

    public boolean shipHit() {
        return shipHit;
    }

    public void touch(float xpos) {
        spaceShip.pos.x = xpos;
        missiles.add(new Missile(spaceShip.pos));
    }

}

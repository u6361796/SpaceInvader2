
package anu.ericm.spaceinvader2;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by ericm on 16/03/16.
 */
public class Missiles extends ArrayList<Missile> {

    public static final float MISSILESTEP = 0.06f;

    public void step() {
        for (Missile b : this) b.pos.y -= MISSILESTEP;
        Iterator<Missile> mi = this.iterator();
        while (mi.hasNext()) {
            Missile m = mi.next();
            if (m.pos.y < Game.MINXY) mi.remove();
        }
    }

    public void draw(Canvas canvas, Paint paint) {
        for (Missile m : this) m.draw(canvas, paint);
    }
}
